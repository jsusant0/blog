# README #

### What is this repository for? ###
* First Programming Assignment - Coursera
* 1.0

### How do I get set up? ###

1. Open git bash, type
   $git clone https://jsusant0@bitbucket.org/jsusant0/blog.git

2. List all files inside the folder to ensure that files are successfully cloned
   $cd blog
   $ls

3. Open command prompt - ruby on rails 
   Navigate to the folder blog/
   example:
   c:\Sites\blog>rails server

4. Open any browser and type 
   http://localhost:3000/
   to ensure that ruby is running
   
   http://localhost:3000/posts
   http://localhost:3000/comments

Good luck!!
Jeffry Susanto